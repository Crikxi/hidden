package fr.crikxi.hidden;


import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;



public class Cooldown {

	static public int cooldown = 50;
	static public HashMap<UUID,Integer> cooldownplayers = new HashMap<UUID,Integer>();
	
	public static boolean Start(Plugin plugin){		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable()
		{
			public void run(){ 	
				for(UUID p : cooldownplayers.keySet()){
					int time = cooldownplayers.get(p);
					time--;
					if(time <= 0){
						cooldownplayers.remove(p);
					}else{
						cooldownplayers.put(p, time);
					}
				}
			}}, 0L,20L);

		return true;
	}
	
}
