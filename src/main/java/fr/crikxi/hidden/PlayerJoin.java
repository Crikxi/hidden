package fr.crikxi.hidden;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		for(UUID player : HideTime.hideplayers.keySet()){
			e.getPlayer().hidePlayer(Bukkit.getPlayer(player));
		}
	}

}
