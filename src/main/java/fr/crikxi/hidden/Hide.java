package fr.crikxi.hidden;



import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Hide implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(Cooldown.cooldownplayers.containsKey(p.getUniqueId())){
				sender.sendMessage(ChatColor.RED + "Il vous reste "+  Cooldown.cooldownplayers.get(p.getUniqueId()) + " secondes de cooldown.");
				return true;
			}
			if(HideTime.hideplayers.containsKey(p.getUniqueId())){
				sender.sendMessage("Vous �tes encore cach� pendant " + HideTime.hideplayers.get(p.getUniqueId()) + " secondes.");
				return true;
			}
			HideTime.hide(p, HideTime.hidetime);
			sender.sendMessage("Vous �tes cach�.");
		}else{
			sender.sendMessage("Vous devez �tre un joueur.");
		}
		return true;
	}


}
