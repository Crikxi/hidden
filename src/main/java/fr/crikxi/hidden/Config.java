package fr.crikxi.hidden;

import org.bukkit.configuration.file.FileConfiguration;


public class Config {

	
	public static void loadconfig(){
		FileConfiguration config = Main.plugin.getConfig();
		if(!config.contains("hidetime")){
			config.set("hidetime", 50);
		}
		if(!config.contains("cooldown")){
			config.set("cooldown", 50);
		}
		Cooldown.cooldown = config.getInt("cooldown");
		HideTime.hidetime = config.getInt("hidetime");
		Main.plugin.saveConfig();;
		
	}
}
