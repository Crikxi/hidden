package fr.crikxi.hidden;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class HideTime {

	static public int hidetime = 50;
	static public HashMap<UUID,Integer> hideplayers = new HashMap<UUID,Integer>();

	public static boolean Start(Plugin plugin){		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable()
		{
			public void run(){ 	
				for(UUID p : hideplayers.keySet()){
					int time = hideplayers.get(p);
					time--;
					if(time <= 0){
						Bukkit.getPlayer(p).sendMessage("Vous n'�tes plus cach�");
						show(Bukkit.getPlayer(p));
					}else{
						hideplayers.put(p, time);
					}
				}
			}}, 0L,20L);

		return true;
	}

	public static void hide(Player player, int time){
		hideplayers.put(player.getUniqueId(),time);
		for(Player p : Bukkit.getOnlinePlayers()){
			p.hidePlayer(player);
		}
	}

	public static void show(Player player){
		if(hideplayers.containsKey(player.getUniqueId()))
				hideplayers.remove(player.getUniqueId());
		for(Player p : Bukkit.getOnlinePlayers()){
			p.showPlayer(player);
		}
		Cooldown.cooldownplayers.put(player.getUniqueId(), Cooldown.cooldown);
	}
}
