package fr.crikxi.hidden;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;



public class Main extends  JavaPlugin{
	
	static public Plugin plugin;
	
	public void onEnable(){
		plugin = this;
		Config.loadconfig();
		getCommand("hide").setExecutor(new Hide());
		HideTime.Start(this);
		Cooldown.Start(this);
		getServer().getPluginManager().registerEvents(new Damage(), this);
		getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
		Config.loadconfig();
	}

}
