package fr.crikxi.hidden;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class Damage implements Listener {

	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onEntityDamage(EntityDamageEvent e)
	{
		if(e.getEntityType() == EntityType.PLAYER){
			Player p = (Player) e.getEntity();
			if(HideTime.hideplayers.containsKey(p.getUniqueId())){
				HideTime.show(p);
				p.sendMessage("Vous n'�tes plus cach�");
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e)
	{
		if(e.getDamager().getType() == EntityType.PLAYER){
			Player p = (Player) e.getDamager();
			if(HideTime.hideplayers.containsKey(p.getUniqueId())){
				HideTime.show(p);
				p.sendMessage("Vous n'�tes plus cach�");
			}
		}
	}
}
